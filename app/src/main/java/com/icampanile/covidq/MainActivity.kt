package com.icampanile.covidq

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.icampanile.covidq.data.common.ControlsApp
import com.icampanile.covidq.data.common.DialogApp
import com.icampanile.covidq.data.common.SharedPreferencesApp
import com.icampanile.covidq.data.database.FirebaseRequest
import com.icampanile.covidq.data.interfaces.OnBackPressedListener
import com.icampanile.covidq.data.model.Quarantine
import com.icampanile.covidq.ui.NewQuarantineActivity


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private var onBackPressedListener: OnBackPressedListener? = null

    private lateinit var textStatusQuarantine: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //App Theme - control theme in the system settings
        AppCompatDelegate.setDefaultNightMode(
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)


        val fab: FloatingActionButton = findViewById(R.id.new_quarantine)
        fab.setOnClickListener {
            val topicsIntent = Intent(this, NewQuarantineActivity::class.java)
            startActivity(topicsIntent)
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        val headerNavView = navView.getHeaderView(0)

        textStatusQuarantine = headerNavView.findViewById(R.id.nav_status_quarantine)


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_map, R.id.nav_quarantines, R.id.nav_privacy), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onStart() {
        super.onStart()

        if (ControlsApp.checkInternetConnection(this)) {
            checkPlayServices()
            checkExistQuarantine()
        } else {
            DialogApp.internetConnectionNotFound(this)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem: MenuItem? = menu?.findItem(R.id.action_search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView: SearchView? = searchItem?.actionView as SearchView

        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        return super.onCreateOptionsMenu(menu)
    }

    private fun checkExistQuarantine(){

        val quarantineID = SharedPreferencesApp.getInstance(this).quarantineID

        if (!quarantineID.isNullOrEmpty()) {
            FirebaseRequest.quarantinesCollection
                .document(quarantineID)
                .get()
                .addOnSuccessListener { document ->

                    if (document.exists()) {
                        val quarantine = document.toObject(Quarantine::class.java)

                        val currentDate = com.google.firebase.Timestamp.now()
                        val diff = quarantine!!.endTime!!.seconds - currentDate.seconds
                        val days: Long = diff / 86400

                        var text = getString(R.string.nav_header_subtitle, days)

                        if (days <= 0){
                            text = getString(R.string.quarantine_ended)
                            SharedPreferencesApp.getInstance(this).cleanSharedPreferences()
                        }

                        textStatusQuarantine.text = text



                    } else {

                        textStatusQuarantine.text = getString(R.string.text_no_quarantine_signalited)
                    }
                }
        } else {

            textStatusQuarantine.text = getString(R.string.text_no_quarantine_signalited)

        }
    }

    private fun checkPlayServices(): Boolean {
        val playServiceRequest = 9000

        val googleAPI = GoogleApiAvailability.getInstance()
        val result = googleAPI.isGooglePlayServicesAvailable(this)
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(
                    this, result,
                    playServiceRequest
                ).show()
            }
            return false
        }
        return true
    }

    fun setOnBackPressedListener(onBackPressedListener: OnBackPressedListener?) {
        this.onBackPressedListener = onBackPressedListener
    }


    @Override
    override fun onBackPressed() {
        if (onBackPressedListener != null)
            onBackPressedListener!!.doBack()
        else
            super.onBackPressed()
    }

}
