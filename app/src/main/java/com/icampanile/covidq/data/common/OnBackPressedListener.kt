package com.icampanile.covidq.data.common

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager

import com.icampanile.covidq.data.interfaces.OnBackPressedListener


class BaseBackPressedListener(private val activity: FragmentActivity) :
    OnBackPressedListener {
    override fun doBack() {
        activity.supportFragmentManager
            .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

}