package com.icampanile.covidq.data.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.icampanile.covidq.R
import com.icampanile.covidq.data.model.Quarantine


class QuarantinesAdapter (
    private var quarantinesList: List<Quarantine>
) : RecyclerView.Adapter<QuarantinesAdapter.TopicHolder>(), Filterable{

    private var quarantinesListFiltered: List<Quarantine> = quarantinesList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TopicHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: TopicHolder, position: Int) {

        val quarantine: Quarantine = quarantinesListFiltered[position]
        holder.bind(quarantine)
    }

    override fun getItemCount(): Int = quarantinesListFiltered.size

    override fun getItemId(position: Int): Long = position.toLong()

    class TopicHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.single_quarantine, parent, false)) {

        private var placeQuarantineView: TextView? = null
        private var statusQuarantineView: TextView? = null
        private var cityQuarantineView: TextView? = null


        init {

            placeQuarantineView = itemView.findViewById(R.id.quarantine_place)
            statusQuarantineView = itemView.findViewById(R.id.quarantine_status)
            cityQuarantineView = itemView.findViewById(R.id.quarantine_city)

        }

        fun bind(
            quarantine: Quarantine
        ) {

            placeQuarantineView?.text = quarantine.state

            cityQuarantineView?.text = quarantine.city

            val currentDate = com.google.firebase.Timestamp.now()
            val diff =  quarantine.endTime!!.seconds - currentDate.seconds
            val days: Long = diff / 86400

            statusQuarantineView?.text = itemView.context.getString(R.string.nav_header_subtitle, days)

        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                quarantinesListFiltered = filterResults.values as List<Quarantine>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase()

                val filterResults = FilterResults()
                filterResults.values = if (queryString==null || queryString.isEmpty())
                    quarantinesList
                else
                    quarantinesList.filter {
                        it.address!!.toLowerCase().contains(queryString) ||
                                it.city!!.toLowerCase().contains(queryString) ||
                                it.state!!.toLowerCase().contains(queryString) ||
                                it.postalCode!!.toLowerCase().contains(queryString)
                    }

                return filterResults
            }
        }
     }

}