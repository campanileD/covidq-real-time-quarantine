package com.icampanile.covidq.data.interfaces

interface OnBackPressedListener {
    fun doBack()
}