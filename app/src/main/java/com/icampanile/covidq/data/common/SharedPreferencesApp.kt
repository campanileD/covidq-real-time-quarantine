package com.icampanile.covidq.data.common

import android.content.Context
import android.content.SharedPreferences


class SharedPreferencesApp(context: Context) {

    //SharedPreferences
    private var prefs: SharedPreferences =
        context.getSharedPreferences(
            "CovidQ_Preferences",
            Context.MODE_PRIVATE)


    companion object {

        private lateinit var jInstance: SharedPreferencesApp

        @Synchronized fun getInstance(context:Context):SharedPreferencesApp {
            jInstance = SharedPreferencesApp(context)
            return jInstance
        }
    }


    var quarantineID: String?
        get() {
            return prefs.getString("quarantineID", "")
        }
        set(quarantineID) {
            prefs.edit().putString("quarantineID", quarantineID!!).apply()
            prefs.edit().apply()
        }

    var latitudeStart: String?
        get() {
            return prefs.getString("latitudeStart", "")
        }
        set(latitude) {
            prefs.edit().putString("latitudeStart", latitude!!).apply()
            prefs.edit().apply()
        }

    var longitudeStart: String?
        get() {
            return prefs.getString("longitudeStart", "")
        }
        set(longitude) {
            prefs.edit().putString("longitudeStart", longitude!!).apply()
            prefs.edit().apply()
        }


    fun cleanSharedPreferences(){
        prefs.edit().clear().apply()
    }
}