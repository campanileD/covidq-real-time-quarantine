package com.icampanile.covidq.data.model

data class Quarantine (

    var address: String? = "",
    var city: String? = "",
    var state: String? = "",
    var country: String? = "",
    var postalCode: String? = "",

    var lat: Double = 0.0,
    var lng: Double = 0.0,
    var startTime: com.google.firebase.Timestamp? = null,
    var endTime: com.google.firebase.Timestamp? = null,

    var isActive: Boolean = true

)