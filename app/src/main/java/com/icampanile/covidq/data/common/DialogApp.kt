package com.icampanile.covidq.data.common

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.icampanile.covidq.MainActivity
import com.icampanile.covidq.R
import com.icampanile.covidq.ui.NewQuarantineActivity
import java.util.*

object DialogApp {


    fun internetConnectionNotFound(activity: AppCompatActivity) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = activity.getString(R.string.text_error_connection)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = activity.getString(R.string.try_again)
        actionBtn.setOnClickListener {
            activity.finish()
            activity.startActivity(activity.intent)
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun internetConnectionNotFound(fragment: Fragment) {

        val dialog = Dialog(fragment.requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = fragment.getString(R.string.text_error_connection)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = fragment.getString(R.string.try_again)
        actionBtn.setOnClickListener {
            val ft: FragmentTransaction =
                fragment.fragmentManager!!.beginTransaction()
            ft.detach(fragment).attach(fragment).commit()
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    fun errorGettingQuarantines(fragment: Fragment) {

        val dialog = Dialog(fragment.requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = fragment.getString(R.string.text_error_reading_documents)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = fragment.getString(R.string.create_new)
        actionBtn.setOnClickListener {

            val intent = Intent (fragment.activity, NewQuarantineActivity::class.java)
            fragment.activity!!.startActivity(intent)
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    fun positionNotFound(activity: AppCompatActivity) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = activity.getString(R.string.text_error_position)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = activity.getString(R.string.try_again)
        actionBtn.setOnClickListener {
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.visibility = View.GONE
        dialog.show()
    }


    fun inputInvalid(activity: AppCompatActivity) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = activity.getString(R.string.text_error_input)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = activity.getString(R.string.button_ok)
        actionBtn.setOnClickListener {
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.visibility = View.GONE
        dialog.show()
    }


    fun createQuarantineError(activity: AppCompatActivity) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = activity.getString(R.string.text_error_create_quarantine)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = activity.getString(R.string.try_again)
        actionBtn.setOnClickListener {
            activity.finish()
            activity.startActivity(activity.intent)
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.visibility = View.GONE
        dialog.show()
    }


    fun createQuarantineSuccess(activity: AppCompatActivity) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = activity.getString(R.string.text_success_create_quarantine)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = activity.getString(R.string.button_ok)
        actionBtn.setOnClickListener {
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.visibility = View.GONE
        dialog.show()
    }


    fun dataUserNotFound(activity: AppCompatActivity) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = activity.getString(R.string.text_not_found_data)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = activity.getString(R.string.button_ok)
        actionBtn.setOnClickListener {
            val intent = Intent (activity, MainActivity::class.java)
            activity.startActivity(intent)
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.visibility = View.GONE
        dialog.show()
    }


    fun dataUserDeleteNotWork(activity: AppCompatActivity) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = activity.getString(R.string.text_error_delete_data)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = activity.getString(R.string.button_ok)
        actionBtn.setOnClickListener {
            val intent = Intent (activity, MainActivity::class.java)
            activity.startActivity(intent)
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.visibility = View.GONE
        dialog.show()
    }

    fun dataUserDeleteSuccess(activity: AppCompatActivity) {

        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_layout)
        Objects.requireNonNull(
            dialog.window!!
                .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        )

        val messageDialog: TextView = dialog.findViewById(R.id.message_dialog)
        messageDialog.text = activity.getString(R.string.text_success_delete_data)

        val actionBtn: Button = dialog.findViewById(R.id.action_btn)
        actionBtn.text = activity.getString(R.string.button_ok)
        actionBtn.setOnClickListener {
            val intent = Intent (activity, MainActivity::class.java)
            activity.startActivity(intent)
            dialog.dismiss()
        }

        val cancelBtn: Button = dialog.findViewById(R.id.cancel_btn)
        cancelBtn.visibility = View.GONE
        dialog.show()
    }


}