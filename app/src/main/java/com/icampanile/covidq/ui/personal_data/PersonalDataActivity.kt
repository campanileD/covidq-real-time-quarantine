package com.icampanile.covidq.ui.personal_data

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.icampanile.covidq.MainActivity
import com.icampanile.covidq.R
import com.icampanile.covidq.data.common.DialogApp
import com.icampanile.covidq.data.common.SharedPreferencesApp
import com.icampanile.covidq.data.database.FirebaseRequest
import com.icampanile.covidq.data.model.Quarantine
import kotlinx.android.synthetic.main.activity_personal_data.*
import java.text.SimpleDateFormat
import java.util.*

class PersonalDataActivity : AppCompatActivity() {

    private  lateinit var sharedPreferencesApp : SharedPreferencesApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_data)


        //App Theme - control theme in the system settings
        AppCompatDelegate.setDefaultNightMode(
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        sharedPreferencesApp = SharedPreferencesApp.getInstance(this)

        request_delete_btn.setOnClickListener { deleteDataUser() }

    }

    override fun onStart() {
        super.onStart()
        checkExistQuarantine()
    }



    private fun checkExistQuarantine(){

        progressBar.visibility = View.VISIBLE

        if (!sharedPreferencesApp.quarantineID.isNullOrEmpty()) {
            FirebaseRequest.quarantinesCollection
                .document(sharedPreferencesApp.quarantineID!!)
                .get()
                .addOnSuccessListener { document ->

                    if (document.exists()) {
                        val quarantine = document.toObject(Quarantine::class.java)

                        quarantine.let {
                            input_latitude.text = quarantine!!.lat.toString()
                            input_longitude.text = quarantine.lng.toString()
                            input_country.text = quarantine.country.toString()
                            input_state.text = quarantine.state.toString()
                            input_city.text = quarantine.city.toString()
                            input_postal_code.text = quarantine.postalCode.toString()
                            input_address.text = quarantine.address.toString()
                            input_start_date.text = convertDate(quarantine.startTime!!)
                            input_end_date.text = convertDate(quarantine.endTime!!)
                        }

                        progressBar.visibility = View.GONE

                    } else {
                        DialogApp.dataUserNotFound(this)
                        progressBar.visibility = View.GONE
                    }
                }
        } else {
            DialogApp.dataUserNotFound(this)
            progressBar.visibility = View.GONE

        }
    }

    private fun convertDate(date: com.google.firebase.Timestamp): String {

        val milliseconds = date.seconds * 1000 + date.nanoseconds / 1000000
        val sdf = SimpleDateFormat("dd/MM/yyyy hh:mm:ss a")
        val netDate = Date(milliseconds)
        return sdf.format(netDate).toString()
    }


    private fun deleteDataUser(){
        progressBar.visibility = View.VISIBLE

        sharedPreferencesApp.quarantineID?.let {
            FirebaseRequest
                .quarantinesCollection
                .document(it)
                .delete()
                .addOnSuccessListener {
                    progressBar.visibility = View.GONE
                   DialogApp.dataUserDeleteSuccess(this)

                }
                .addOnFailureListener {

                    progressBar.visibility = View.GONE
                       DialogApp.dataUserDeleteNotWork(this)

                }
        }

    }


    override fun onSupportNavigateUp(): Boolean {

        val intent = Intent (this, MainActivity::class.java)
        startActivity(intent)

        return true
    }
}
