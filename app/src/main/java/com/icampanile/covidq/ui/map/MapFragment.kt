package com.icampanile.covidq.ui.map

import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.TileOverlayOptions
import com.google.firebase.firestore.GeoPoint
import com.google.maps.android.heatmaps.HeatmapTileProvider
import com.icampanile.covidq.MainActivity
import com.icampanile.covidq.R
import com.icampanile.covidq.data.common.BaseBackPressedListener
import com.icampanile.covidq.data.common.ControlsApp
import com.icampanile.covidq.data.common.DialogApp
import com.icampanile.covidq.data.common.SharedPreferencesApp
import com.icampanile.covidq.data.database.FirebaseRequest
import com.icampanile.covidq.data.model.Quarantine
import kotlinx.android.synthetic.main.fragment_map.*


class MapFragment : Fragment(), OnMapReadyCallback {

    //Var Map
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    //Component Fragment
    private lateinit var myMap: GoogleMap

    private lateinit var sharedPreferencesApp : SharedPreferencesApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val activity = activity
        (activity as MainActivity).setOnBackPressedListener(BaseBackPressedListener(activity))

        val root = inflater.inflate(R.layout.fragment_map, container, false)


        sharedPreferencesApp = SharedPreferencesApp.getInstance(requireContext())

        //Google Map
        mFusedLocationClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())

        val mapFragment :
                SupportMapFragment = childFragmentManager.findFragmentById(R.id.map_view) as SupportMapFragment
        mapFragment.getMapAsync(this)

        return root
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val mSearchMenuItem = menu.findItem(R.id.action_search)
        val searchView =
            mSearchMenuItem.actionView as SearchView

        // listening to search query text change
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {

                // filter recycler view when query submitted
                val position = getLocationFromAddress(query)
                myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15F))  //move camera to location

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

        })
    }

    fun getLocationFromAddress(strAddress: String): LatLng? {

        val coder =  Geocoder(context)
        val address: List<Address>
        val p1: GeoPoint? = null

        address = coder.getFromLocationName(strAddress,5)
        if (address==null) {
            return null
        }
        val location: Address = address[0]

        Log.d("TEST ADDRESS", address[0].toString())

        return LatLng( location.latitude, location.longitude)
    }

    //////////////////// GOOGLE MAP /////////////////////////////////////
    //Google Map Functions
    override fun onMapReady(map: GoogleMap) {
        progressBar.visibility = View.VISIBLE


        val latitudeStart: Double = if (sharedPreferencesApp.latitudeStart.isNullOrEmpty()) 41.89193
        else sharedPreferencesApp.latitudeStart!!.toDouble()

        val longitudeStart: Double = if (sharedPreferencesApp.longitudeStart.isNullOrEmpty()) 12.51133
        else sharedPreferencesApp.longitudeStart!!.toDouble()

        Log.d("TEST", "$latitudeStart | $longitudeStart")

        map.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(context, R.raw.mapstyle)
        )
        myMap = map
        map.uiSettings.isMyLocationButtonEnabled = true
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitudeStart, longitudeStart), 5.0f))

        if (ControlsApp.checkInternetConnection(context!!)) {
            getQuarantines()
        } else{
            DialogApp.internetConnectionNotFound(this)
        }
    }

    private fun getQuarantines() {
        progressBar.visibility = View.VISIBLE

        val list: MutableList<LatLng> = ArrayList()

        FirebaseRequest.quarantinesCollection
            .get()
            .addOnSuccessListener { documents ->
                if (!documents.isEmpty) {
                    for (document in documents) {
                        val quarantine = document.toObject(Quarantine::class.java)

                        if (quarantine.isActive) {
                            val locationQuarantine = LatLng(quarantine.lat, quarantine.lng)
                            list.add(locationQuarantine)
                        }
                    }

                    // Create a heat map tile provider, passing it the latlngs of the police stations.
                    val mProvider = HeatmapTileProvider.Builder()
                        .data(list)
                        .build()
                    // Add a tile overlay to the map, using the heat map tile provider.
                    var mOverlay =
                        myMap.addTileOverlay(TileOverlayOptions().tileProvider(mProvider))
                } else{

                    DialogApp.errorGettingQuarantines(this)

                }
                progressBar.visibility = View.GONE
            }
            .addOnFailureListener { exception ->
                DialogApp.errorGettingQuarantines(this)
                progressBar.visibility = View.GONE
            }

    }


}
