package com.icampanile.covidq.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import com.icampanile.covidq.MainActivity
import com.icampanile.covidq.R
import com.icampanile.covidq.data.common.DialogApp
import com.icampanile.covidq.data.common.SharedPreferencesApp
import com.icampanile.covidq.data.database.FirebaseRequest
import com.icampanile.covidq.data.model.Quarantine
import kotlinx.android.synthetic.main.activity_new_quarantine.*
import java.util.*


class NewQuarantineActivity : AppCompatActivity() {

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val permissionId = 42
    private val handler = Handler()

    private val quarantine = Quarantine()

    private lateinit var sharedPreferencesApp: SharedPreferencesApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_quarantine)

        sharedPreferencesApp = SharedPreferencesApp.getInstance(this)

        //App Theme - control theme in the system settings
        AppCompatDelegate.setDefaultNightMode(
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.title = getString(R.string.new_segnaling)

        //Google Map
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        get_position_btn.setOnClickListener{ getLastLocation() }

        start_quarantine_btn.setOnClickListener{ checkInputPosition() }
    }

    override fun onStart() {
        super.onStart()
        checkExistQuarantine()
    }

    private fun getLastLocation() {

        progressBar.visibility = View.VISIBLE

        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    val location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        setPlaceQuarantine(location.latitude, location.longitude)
                    }
                }
            } else {
                DialogApp.positionNotFound(this)

            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val location: Location = locationResult.lastLocation

            setPlaceQuarantine(location.latitude, location.longitude)

        }
    }

    private fun setPlaceQuarantine(newLat: Double, newLng: Double){

        quarantine.lat = newLat
        quarantine.lng = newLng

        sharedPreferencesApp.latitudeStart = newLat.toString()
        sharedPreferencesApp.longitudeStart = newLng.toString()

        val addresses: List<Address>
        val geocoder = Geocoder(this, Locale.getDefault())

        addresses = geocoder.getFromLocation(
            newLat,
            newLng,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5


        val address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        val splitAddress: List<String> = address.split(", ") // Splits address where there is a space, creating array with 5 index. Index 4 should be CITY!

        quarantine.address = splitAddress[0] + " " + splitAddress[1]

        quarantine.city = addresses[0].locality
        quarantine.state = addresses[0].adminArea
        quarantine.country = addresses[0].countryName
        quarantine.postalCode = addresses[0].postalCode

        position_text.text = "latitude: $newLat - longitude: $newLng"

        progressBar.visibility = View.GONE
        get_position_btn.setImageResource(R.drawable.ic_check)
        get_position_btn.isEnabled = false

    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        progressBar.visibility = View.GONE

        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            permissionId
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == permissionId) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkInputPosition() {

        if (quarantine.lat != 0.0 && quarantine.lng != 0.0){

            quarantine.startTime = com.google.firebase.Timestamp.now()

            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, 14)

            quarantine.endTime = com.google.firebase.Timestamp(calendar.time)

            newQuarantine()
        } else {
            DialogApp.inputInvalid(this)
        }

    }

    private fun timerQ(endDate: com.google.firebase.Timestamp) {

        timer_text.visibility = View.VISIBLE

        handler.post(object : Runnable {
            override fun run() {
                // Keep the postDelayed before the updateTime(), so when the event ends, the handler will stop too.
                handler.postDelayed(this, 1000)

                val currentDate = com.google.firebase.Timestamp.now()

                val diff =  endDate.seconds - currentDate.seconds

                val sec: Long = diff % 60
                val minutes: Long = diff % 3600 / 60
                val hours: Long = diff % 86400 / 3600
                val days: Long = diff / 86400

                timer_text.text = "- $days day $hours h $minutes m $sec s"

                endEvent(currentDate, endDate)


            }
        })
    }

    private fun endEvent(currentdate: com.google.firebase.Timestamp, eventdate: com.google.firebase.Timestamp) {
        if (currentdate.seconds >= eventdate.seconds) {
            timer_text.text = getString(R.string.end_quarantine_text)
            //Stop Handler
            handler.removeMessages(0)
        }
    }

    private fun newQuarantine(){

            FirebaseRequest.quarantinesCollection
            .add(quarantine)
            .addOnSuccessListener { documentReference ->
                SharedPreferencesApp.getInstance(this).quarantineID = documentReference.id
                progressBar.visibility = View.GONE
                start_quarantine_btn.setImageResource(R.drawable.ic_check)
                start_quarantine_btn.isEnabled = false

                DialogApp.createQuarantineSuccess(this)
                timerQ(quarantine.endTime!!)
            }
            .addOnFailureListener { e ->
                progressBar.visibility = View.GONE
                DialogApp.createQuarantineError(this)

            }
    }

    private fun checkExistQuarantine(){
        progressBar.visibility = View.VISIBLE

        val quarantineID = SharedPreferencesApp.getInstance(this).quarantineID

        if (!quarantineID.isNullOrEmpty()) {
            FirebaseRequest.quarantinesCollection
                .document(quarantineID)
                .get()
                .addOnSuccessListener { document ->
                    if (document.exists()) {
                        progressBar.visibility = View.GONE

                        start_quarantine_btn.setImageResource(R.drawable.ic_check)
                        start_quarantine_btn.isEnabled = false

                        get_position_btn.setImageResource(R.drawable.ic_check)
                        get_position_btn.isEnabled = false

                        val quarantine = document.toObject(Quarantine::class.java)
                        timerQ(quarantine!!.endTime!!)

                    } else {
                        progressBar.visibility = View.GONE

                    }
                }
        } else {
            progressBar.visibility = View.GONE

        }
    }

    override fun onSupportNavigateUp(): Boolean {

        val intent = Intent (this, MainActivity::class.java)
        startActivity(intent)

        return true
    }

}
