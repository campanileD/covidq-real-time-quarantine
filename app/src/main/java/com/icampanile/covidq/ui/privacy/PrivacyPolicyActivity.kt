package com.icampanile.covidq.ui.privacy

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.icampanile.covidq.MainActivity
import com.icampanile.covidq.R

class PrivacyPolicyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)

        //App Theme - control theme in the system settings
        AppCompatDelegate.setDefaultNightMode(
            AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)


    }

    override fun onSupportNavigateUp(): Boolean {

        val intent = Intent (this, MainActivity::class.java)
        startActivity(intent)

        return true
    }
}
