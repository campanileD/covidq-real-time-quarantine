package com.icampanile.covidq.ui.quarantines

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.Query
import com.icampanile.covidq.MainActivity
import com.icampanile.covidq.R
import com.icampanile.covidq.data.adapter.QuarantinesAdapter
import com.icampanile.covidq.data.common.BaseBackPressedListener
import com.icampanile.covidq.data.common.DialogApp
import com.icampanile.covidq.data.database.FirebaseRequest
import com.icampanile.covidq.data.model.Quarantine


class QuarantinesFragment : Fragment() {

    //Topics RV
    private var quarantinesAdapter: QuarantinesAdapter? = null
    private val quarantinesList: MutableList<Quarantine> = ArrayList()

    private lateinit var progressBarFragment: ProgressBar
    private lateinit var rvQuarantines: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val activity = activity
        (activity as MainActivity).setOnBackPressedListener(BaseBackPressedListener(activity))

        val root = inflater.inflate(R.layout.fragment_quarantines, container, false)

        progressBarFragment = root.findViewById(R.id.progressBar)
        rvQuarantines = root.findViewById(R.id.rv_quarantines)

        //Populate RV
        getQuarantines()

        rvQuarantines.layoutManager = LinearLayoutManager(context)
        rvQuarantines.setHasFixedSize(true)

        return root
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val mSearchMenuItem = menu.findItem(R.id.action_search)
        val searchView =
            mSearchMenuItem.actionView as SearchView


        // listening to search query text change
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                quarantinesAdapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                quarantinesAdapter?.filter?.filter(query)
                return false
            }
        })
    }

    /** Load personal topics */
    private fun getQuarantines() {
        progressBarFragment.visibility = View.VISIBLE

        FirebaseRequest.quarantinesCollection
            .orderBy("startTime", Query.Direction.ASCENDING)
            .get()
            .addOnSuccessListener { documents ->
                if (!documents.isEmpty) {
                    for (document in documents) {
                        val quarantine = document.toObject(Quarantine::class.java)
                        quarantinesList.add(quarantine)
                    }

                    quarantinesAdapter = QuarantinesAdapter(quarantinesList)

                    rvQuarantines.adapter = quarantinesAdapter
                    progressBarFragment.visibility = View.GONE

                } else {
                    progressBarFragment.visibility = View.GONE
                    DialogApp.errorGettingQuarantines(this)
                }

            }
            .addOnFailureListener { exception ->
                DialogApp.errorGettingQuarantines(this)
                progressBarFragment.visibility = View.GONE
            }
    }


}
