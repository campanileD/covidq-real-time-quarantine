CovidQ is an app developed to create an ongoing quarantine map due to the Covid-19 emergency.
To help law enforcement, hospitals and our mayors, you need to know in real time the location where the quarantine is being spent.
The app does not require any personal data, only your location.

Database: Firestore
Map: Google Maps Heat Map
Language: Kotlin
IDE: Android Studio

Uncomment this code in the Gradle App

//Firestore
//implementation 'com.google.firebase:firebase-firestore:21.4.1'

//Map
//implementation 'com.google.android.gms:play-services-maps:17.0.0'
//implementation 'com.google.android.gms:play-services-location:17.0.0'
//implementation 'com.google.maps.android:android-maps-utils:0.5'
